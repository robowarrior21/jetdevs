package com.example.jetdevs

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.jetdevs.network.ApiClient
import com.example.jetdevs.network.ApiService
import com.example.jetdevs.room.AppDatabase
import com.example.jetdevs.viewModel.MainViewModel
import com.example.jetdevs.viewModel.ViewModelFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job

open class BaseActivity : AppCompatActivity() {

    internal val name = BaseActivity::class.java.simpleName

    private val job = Job()
    internal val scopeMainThread = CoroutineScope(job + Dispatchers.Main)

    open suspend fun getViewModel(): MainViewModel = ViewModelProviders.of(
        this,
        ViewModelFactory(
            ApiClient().apiClient().create(ApiService::class.java)
        )
    )
        .get(MainViewModel::class.java)

    open suspend fun getAppDatabase(): AppDatabase = Room.databaseBuilder(
        applicationContext,
        AppDatabase::class.java, "demo_jet_dev"
    ).build()

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }
}