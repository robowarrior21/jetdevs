package com.example.jetdevs

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import com.example.jetdevs.model.LoginRequestParams
import com.example.jetdevs.room.AppDatabase
import com.example.jetdevs.room.User
import com.example.jetdevs.utils.Status
import com.example.jetdevs.utils.Utility.showError
import com.example.jetdevs.viewModel.MainViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.view.*
import kotlinx.coroutines.launch

class LoginActivity : BaseActivity(), View.OnClickListener {

    private lateinit var viewModel: MainViewModel
    private lateinit var roomDb: AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        scopeMainThread.launch {
            viewModel = getViewModel()
            roomDb = getAppDatabase()
            roomDb.userDao()
        }

        scopeMainThread.launch {
            login_btn.setOnClickListener(this@LoginActivity)
        }

    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.login_btn -> {
                when {
                    username.text.toString()
                        .isEmpty() -> username.showError("Please enter your username")
                    password.text.toString()
                        .isEmpty() -> password.showError("Please enter your password")
                    else -> {
                        viewModel.getUserLogin(
                            LoginRequestParams(
                                username_edit_text.username.text.toString(),
                                password_edit_text.password.text.toString()
                            )
                        ).observe(
                            this, Observer {
                                it.let { resource ->
                                    when (resource.status) {
                                        Status.SUCCESS -> {

                                            it.data?.let { it1->
                                                Log.d("TAG", "onClick: " + it)

                                                val userDao = roomDb.userDao()

                                                userDao.insert(User(it1.user.userId,it1.user.userName,password_edit_text.password.text.toString()))
                                                val abc = Snackbar.make(
                                                    constraint,
                                                    "Logged In Successfully" ,
                                                    Snackbar.LENGTH_LONG
                                                )
                                                abc.show()
                                            }

                                        }
                                        Status.ERROR -> {
                                            val abc = Snackbar.make(
                                                constraint,
                                                "" + it.data.toString(),
                                                Snackbar.LENGTH_LONG
                                            )
                                            abc.show()
                                        }
                                        Status.LOADING -> {

                                        }
                                    }
                                }
                            })
                    }
                }
            }
        }
    }
}