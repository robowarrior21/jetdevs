package com.example.jetdevs.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.jetdevs.model.LoginRequestParams
import com.example.jetdevs.network.ApiService
import com.example.jetdevs.utils.Resource
import kotlinx.coroutines.Dispatchers

class MainViewModel(private val apiService: ApiService) : ViewModel() {

    // user login
    fun getUserLogin(loginRequestParams: LoginRequestParams) =
        liveData(Dispatchers.IO) {
            emit(Resource.loading(data = null))
            try {
                emit(Resource.success(data = apiService.getUserLogin(loginRequestParams)))
            } catch (exception: Exception) {
                emit(Resource.error(data = null, message = exception.message ?: "Error occurred"))
            }
        }


}