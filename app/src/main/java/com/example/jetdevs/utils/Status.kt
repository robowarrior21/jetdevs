package com.example.jetdevs.utils

enum class Status {
    SUCCESS,
    LOADING,
    ERROR
}