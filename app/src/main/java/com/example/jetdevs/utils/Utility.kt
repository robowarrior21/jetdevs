package com.example.jetdevs.utils

import android.view.View
import android.widget.EditText

object Utility {

    fun EditText.showError(text: String){
        this.error = text
    }
}