package com.example.jetdevs.model

data class User(
    val userId: Int,
    val userName: String
)