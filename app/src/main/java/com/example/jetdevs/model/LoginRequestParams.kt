package com.example.jetdevs.model

data class LoginRequestParams(val username: String,val password: String)