package com.example.jetdevs.model

data class LoginResponse(
    val errorCode: String,
    val errorMessage: String,
    val user: User
)