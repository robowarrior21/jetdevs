package com.example.jetdevs.network

import com.example.jetdevs.model.LoginRequestParams
import com.example.jetdevs.model.LoginResponse
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiService {

    @POST("login")
    suspend fun getUserLogin(
        @Body loginRequestParams: LoginRequestParams
    ): LoginResponse
}